NAME=scepter
VERSION=0.1

srcdir=./

SHELL=/bin/sh

GIT=git
GITFLAGS=--git-dir=${srcdir}.git/

LUA=lua
LUAFLAGS=

LUACHECK=luacheck
LUACHECKFLAGS=--config ${srcdir}etc/luacheckrc.lua

XMLLINT=xmllint
XMLLINTFLAGS=--schema ${srcdir}share/xml/FrameXML/UI.xsd

INSTALL=unzip -d ${DESTDIR}Interface/AddOns

all: | check

dist: ${NAME}-${VERSION}.zip | check

${NAME}-${VERSION}.zip:
	${GIT} ${GITFLAGS} archive --format=zip --prefix=${NAME}/ --format=zip --output=${NAME}-${VERSION}.zip HEAD

install: ${NAME}-${VERSION}.zip
	${INSTALL} ${NAME}-${VERSION}.zip

.PHONY=check-xml
check-xml:
	${XMLLINT} --noout ${XMLLINTFLAGS} ${srcdir}src/*.xml

.PHONY=check-lua
check-lua:
	${LUACHECK} ${LUACHECKFLAGS} ${srcdir}src/ ${srcdir}local/

.PHONY=check
check: check-lua check-xml
