--[[ http://luacheck.readthedocs.io/en/stable/config.html ]]--

--[[ Functions defined in World of Warcraft client Lua environment. It is
distinct from default Lua 5.1 environment. For example, `os` package is
excluded. Only globals used by this project are included, therefore the list is
only a subset and not exhaustive. ]]--

--[[ Functions defined since Wrath or earlier. ]]--
stds.wrath = {
	read_globals = {
		'ClearOverrideBindings',
		'CreateFrame',
		'GetSpellInfo',
		'assert',
		'date',
		'error',
		'format',
		'math',
		'pairs',
		'print',
		'select',
		'string',
		'strtrim',
		'tContains',
		'table',
		'time',
		'tostring',
		'type',
		'unpack',
	},
	globals = {
		'_G',
	},
}

--[[ Functions and globals defined by authors of this add-on. This list is not exhaustive. ]]--

stds.scepter = {
	read_globals = {
		'ScepterFrame',
		'ScepterProfileButton1',
		'ScepterProfileButton2',
		'ScepterProfileButton3',
		'ScepterSecureHandlerClickButton',
	},
	globals = {
		'BINDING_HEADER_SCEPTER',
		'Scepter',
		'ScepterProfile1',
		'ScepterProfile2',
		'ScepterProfile3',
		'ScepterProfileName1',
		'ScepterProfileName2',
		'ScepterProfileName3',
	},
}

std = 'wrath+scepter'
