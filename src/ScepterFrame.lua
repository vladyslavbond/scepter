local Scepter = Scepter

local function init(self)
	assert(self ~= nil)
	assert('ScepterFrame' == self:GetName())

	self:UnregisterAllEvents()
	self:SetScript('OnEvent', nil)

	local t = {
		ScepterProfileButton1,
		ScepterProfileButton2,
		ScepterProfileButton3,
	}

	local confMap = {
		{ScepterProfileName1, ScepterProfile1},
		{ScepterProfileName2, ScepterProfile2},
		{ScepterProfileName3, ScepterProfile3},
	}

	local i = 0
	while(i < #t) do
		i = i + 1
		local profileButton = t[i]
		assert(profileButton ~= nil)

		local confName = confMap[i][1] or ('Profile ' .. tostring(i))
		local conf = confMap[i][2]
		if not conf then
			print(date('%X') .. ' [Scepter]: could not find configuration for profile ' .. tostring(i))
			conf = {}
		end
		Scepter.profileButtonApply(profileButton, confName, conf)
	end
end

function Scepter.main(self)
	assert(self ~= nil)
	assert('ScepterFrame' == self:GetName())

	self:RegisterEvent('PLAYER_LOGIN')
	self:SetScript('OnEvent', init)

	--[[ TODO Add globalization ]]--
	BINDING_HEADER_SCEPTER = 'Scepter'
	_G['BINDING_NAME_CLICK ScepterProfileButton1:LeftButton'] = 'Profile 1'
	_G['BINDING_NAME_CLICK ScepterProfileButton2:LeftButton'] = 'Profile 2'
	_G['BINDING_NAME_CLICK ScepterProfileButton3:LeftButton'] = 'Profile 3'

	--[[ TODO Add configurable bindings, at least for toggling the spoiler. ]]--
	--[[SetBindingClick('`', 'ScepterProfileButton1')]]--
end
