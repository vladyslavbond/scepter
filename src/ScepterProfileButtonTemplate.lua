local CreateFrame = CreateFrame

function Scepter.profileButtonMain(profileButton)
	assert(profileButton~= nil)

	--[[ Initialize children spell buttons. ]]--

	local m =profileButton:GetName()
	local i = 0
	while(i < 144) do
		i = i + 1
		local n = string.format('%sSpellButton%03d', m, i)
		local b = _G[n]
		if not b then
			b = CreateFrame('BUTTON', n,profileButton, 'ScepterSpellButtonTemplate')
		end
		assert(b ~= nil)
	end

	--[[ Configure profile button toggling. ]]--

	local secureHandler = ScepterSecureHandlerClickButton

	--[[ When the frame is shown, the user may toggle it by pressing the
	ESCAPE key. ]]--

	--[[ Only one profile may be active at a time. That is, only one
	profile button may be shown at a time. ]]--

	secureHandler:WrapScript(profileButton, 'OnShow', [[
		-- Bind ESCAPE key.

		local overrideKeyFlag = true
		local bindingKey = 'ESCAPE'
		local bindingOwner = self
		self:SetBindingClick(overrideKeyFlag, bindingKey, bindingOwner)
		self:SetBindingClick(overrideKeyFlag, 'CTRL-W', bindingOwner)

		-- Hide sibling profile buttons (AKA spoilers, bars, pages).

		local scepterFrame = owner:GetFrameRef('ScepterFrame')
		local t = scepterFrame:GetChildList(newtable())
		local i = 0
		while(i < #t or i < 8192) do
			i = i + 1
			local f = t[i]
			if f and f ~= self then
				f:Hide()
			end
		end
		--print(self:GetName(), 'IsShown', self:IsShown())
	]])

	secureHandler:WrapScript(profileButton, 'OnHide', [[
		self:ClearBindings()
		--print(self:GetName(), 'IsShown', self:IsShown())
	]])

	secureHandler:WrapScript(profileButton, 'OnClick', [[
		if self:IsShown() then
			self:Hide()
		else
			self:Show()
		end
		--print('click', self:GetName())
	]])

	assert(ScepterFrame ~= nil)
	secureHandler:SetFrameRef('ScepterFrame', ScepterFrame)
end

function Scepter.profileButtonApply(profileButton, confName, conf)
	assert(profileButton ~= nil)

	assert(confName ~= nil)

	assert(conf ~= nil)
	assert('table' == type(conf))

	local t = {profileButton:GetChildren()}

	assert(#conf <= #t)

	local i = 0
	while(i < #conf or i < 8192) do
		i = i + 1

		local b = t[i]
		assert(b ~= nil, 'not enough buttons to map all bindings')

		local record = conf[i]
		if not record then
			break
		end

		assert(record ~= nil)
		assert('table' == type(record))
		assert(3 == #record)

		Scepter.spellButtonApply(b, unpack(record))
	end

	while(i < #t) do
		local b = t[i]
		assert(b ~= nil)

		Scepter.spellButtonUnapply(b)

		i = i + 1
	end

	--[[ Fluff. Adjust text labels in option panel and such. ]]--

	local label = _G[profileButton:GetName() .. 'Text']
	if label then
		label:SetText(confName)
	end

	local bindingHeader = string.format('BINDING_NAME_CLICK %s:LeftButton', profileButton:GetName())
	if _G[bindingHeader] then
		_G[bindingHeader] = confName
	end
end
