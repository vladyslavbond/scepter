function Scepter.spellButtonMain(spellButton)
	assert(spellButton ~= nil)

	spellButton:SetAttribute('*type*', 'spell')
	spellButton:SetAttribute('checkselfcast', false)
	spellButton:SetAttribute('checkfocuscast', false)

	--[[ When the button is shown, override key bindings to perform the
	action associated with this button. ]]--

	--[[ When the button's parent is toggled (shown or hidden), the button
	itself is toggled accordingly. ]]--

	local secureHandler = ScepterSecureHandlerClickButton
	assert(secureHandler ~= nil)

	secureHandler:WrapScript(spellButton, 'OnShow', [[
		local overrideFlag = true
		local key = self:GetAttribute('scepter-key')
		local clickTarget = self
		if key then
			self:SetBindingClick(overrideFlag, key, clickTarget)
		--else
			--print('[Scepter]: warning: could not find binding key for button ' .. self:GetName())
		end
	]])

	secureHandler:WrapScript(spellButton, 'OnHide', [[
		self:ClearBindings()
	]])

	return spellButton
end

function Scepter.spellButtonApply(spellButton, spellName, targetUnit, bindingKey)
	assert(spellButton ~= nil)

	assert(spellName ~= nil)
	assert('string' == type(spellName))
	spellName = strtrim(spellName )
	assert(string.len(spellName) >= 1)
	assert(string.len(spellName) <= 256)

	if not GetSpellInfo(spellName) then
		print(date('%X') .. ' [Scepter]: warning: unknown spell "' .. spellName .. '"')
	end

	assert(targetUnit ~= nil)
	assert('string' == type(targetUnit))
	targetUnit = string.lower(strtrim(targetUnit))
	assert(string.len(targetUnit) >= 1)
	assert(string.len(targetUnit) <= 256)

	assert(bindingKey ~= nil)
	assert('string' == type(bindingKey))
	bindingKey = strtrim(bindingKey)
	assert(string.len(bindingKey) >= 1)
	assert(string.len(bindingKey) <= 256)

	spellButton:SetAttribute('*type*', 'spell')
	spellButton:SetAttribute('unit', targetUnit)
	spellButton:SetAttribute('spell', spellName)
	spellButton:SetAttribute('checkselfcast', false)
	spellButton:SetAttribute('checkfocuscast', false)

	spellButton:SetAttribute('scepter-key', bindingKey)

	return spellButton
end

function Scepter.spellButtonUnapply(spellButton)
	assert(spellButton ~= nil)

	spellButton:SetAttribute('unit', 'none')
	spellButton:SetAttribute('spell', nil)
	ClearOverrideBindings(spellButton)
end
